const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = merge(common, {
  mode: "development",
  plugins: [
    new CopyPlugin([
      {
        from: "./src/importmap.json",
      },
    ]),
  ],
});
