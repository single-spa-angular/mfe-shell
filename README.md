# Proof of Concept for MicroFrontends using Single-Spa

## Micro Frontends

In this group there are 3 projects. These were created using ng new with minimal custom code. All should be running in there own shell. No port conflict as these have been configured to run on separate ports.

- [Navigation](https://gitlab.com/single-spa-angular/mfe-navi)
- [Location](https://gitlab.com/single-spa-angular/mfe-locn)
- [Logs](https://gitlab.com/single-spa-angular/mfe-logs)

In a new shell at the root of each project and respectively run ...

`npm run serve:single-spa:nav`

`npm run serve:single-spa:logs`

`npm run serve:single-spa:locn`

In a new shell at root of this this project and run ...

`npx start -s`

Open browser and experiment!

## How to create / add your own mfe

[Single-Spa Documentation for Angular](https://single-spa.js.org/docs/ecosystem-angular)

- Apps were created using following steps.
- Assuming latest LTS npm, node and ng-cli are installed.
- Note: replace $1 with the name of the application you want to create.

```bash
echo 'VVV current config VVV'
npm config ls -l
npm --version
node --version
ng version
echo '============\n\n\n'

# https://angular.io/analytics ... we are just testing!
ng analytics off

ng new $1 --routing --prefix $1 --skipInstall=true --style=sass
cd $1
npm install

#check point
git add .
git commit -a -m 'init'

# avoid bug currently present
# ...has missing dependencies:
# - single-spa
npm install --save single-spa single-spa-angular
ng add single-spa-angular --defaults=true
npm install

#check point
git add .
git commit -a -m 'ng add single-spa-angular'
```

### Few more steps

- [Configure Routes](https://single-spa.js.org/docs/ecosystem-angular#configure-routes)

- Clean up HTML src/app/app.component.html
