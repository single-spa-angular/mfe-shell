import { registerApplication, start } from "single-spa";
registerApplication(
  "navi",
  () => System.import("navi"),
  () => true
);

waitForSideNavContent();

function waitForSideNavContent() {
  if (document.querySelector("mat-sidenav-content") === null) {
    console.log("waiting for sidenav-content ..");
    setTimeout(waitForSideNavContent, 200);
  } else {
    registerApplication(
      "locn",
      () => System.import("locn"),
      (location) => location.pathname.startsWith("/locn")
    );
    registerApplication(
      "logs",
      () => System.import("logs"),
      (location) => location.pathname.startsWith("/logs")
    );
    }
}

start();
